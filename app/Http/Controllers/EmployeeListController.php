<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use DB;

class EmployeeListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.employee.employee_index');
        //
    }

    public function load_employees(){
        $emp = DB::table('users_table as ut')
        ->leftJoin('users_details as ud', 'ut.id', '=', 'ud.users_id');
        return DataTables::of($emp)
        ->addColumn('action', function ($emp){
            return '<a class="btn btn-rounded btn-success btn-xs" href="#" id="employee_view" data-employee-id="'.$emp->id.'"><i class="fa fa-eye"></i></a> <a class="btn btn-rounded btn-info btn-xs" href="#" id="employee_edit" data-employee-id="'.$emp->id.'"><i class="fa fa-edit"></i></a> <a class="btn btn-rounded btn-danger btn-xs mt-1" href="#" id="employee_deductions" data-employee-id="'.$emp->id.'"><i class="fa fa-list"></i> Deductions</a>';
        })
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd($id);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $update = DB::table('users_details')
        ->where('users_id', $request->employee_id)
        ->update([
            'mobile_number' => $request->mobile_number,
            'address' => $request->address,
            'email_address' => $request->email_address
        ]);

        $update_users_details = DB::table('users_details')
        ->updateOrInsert(
            [
                'users_id' => $request->employee_id
            ]
            ,[
            'mobile_number' => $request->mobile_number,
            'address' => $request->address,
            'email_address' => $request->email_address,
            'salary_rate' => $request->salary_rate
            ]
        );

        $update_users_table = DB::table('users_table')
        ->where('id', $request->employee_id)
        ->update([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name
        ]);
        
        return redirect()->back();
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function employee_deductions(Request $request){
        $update_users_details = DB::table('users_deductions')
        ->updateOrInsert(
            [
                'user_id' => $request->employee_id
            ]
            ,[
            'pagibig' => $request->pagibig,
            'philhealth' => $request->philhealth,
            'sss' => $request->sss,
            'total_deductions' => $request->total_deductions
            ]
        );

        return redirect()->back();
    }
}
