<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class PayrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee_list = DB::table('users_table')->get();
        return view('pages.payroll.payroll_index', compact('employee_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd('sdsd');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function create_payroll(Request $request){
        $date1 = date_create($request->start_date);
        $date2 = date_create($request->end_date);
        
        //difference between two dates
        $diff = date_diff($date1,$date2);
        
        //count days
        $working_days = $diff->format("%a");

        $get_work_days = DB::table('attendance_logs')
        ->select(DB::raw('DATE(date_added)'))
        ->whereBetween('date_added', array($request->start_date, $request->end_date))
        ->where('user_id', $request->employee)
        ->groupBy(DB::raw('DATE(date_added)'))
        ->get();
        
        $present_work_days = count($get_work_days);

        $absent = (double)$working_days - (double)$present_work_days;

        $absent_deduction = $absent * 100;

        //get details
        $get_salary = DB::table('users_details')
        ->where('users_id', $request->employee)
        ->first();

        $salary = $get_salary->salary_rate;

        $get_deductions = DB::table('users_deductions')
        ->where('user_id', $request->employee)
        ->first();

        $total_deductions = (double) $get_deductions->total_deductions + (double) $absent_deduction;

        $net_pay = (double)$salary - (double)$total_deductions;

        $html = '';

        $html .= '
        <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Payroll Date</th>
                                <th>Gross Earnings</th>
                                <th colspan="6">Deductions</th>
                                <th>Total Deductions</th>
                                <th>Net Pay</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">'.$request->start_date.' to '.$request->end_date.'</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">&#8369; '.$salary.'</td>
                                <td><strong>No. of Absent</strong></td>
                                <td><strong>Deduction per Absent</strong></td>
                                <td><strong>No. of Late</strong></td>
                                <td><strong>PAGIBIG</strong></td>
                                <td><strong>Phil Health</strong></td>
                                <td><strong>SSS</strong></td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">&#8369; '.$total_deductions.'</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">&#8369; '.$net_pay.'</td>
                            </tr>
                            <tr>
                                <td>'.$absent.'</td>
                                <td>&#8369; 100</td>
                                <td>1</td>
                                <td>&#8369; '.$get_deductions->pagibig.'</td>
                                <td>&#8369; '.$get_deductions->philhealth.'</td>
                                <td>&#8369;  '.$get_deductions->sss.'</td>
                            </tr>
                        </tbody>
                    </table>
                ';

                return $html;
    }
}
