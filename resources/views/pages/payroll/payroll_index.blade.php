@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <h5>Create Payroll</h5>
        </div>
        <div class="card-body">
            <form method="post" action="{{URL('employeepayroll/create_payroll')}}" id="process_payroll">
            {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <strong><label>Select Employee</label></strong>
                            <input list="employees" name="employee" class="form-control" placeholder="Type Employee Name">
                            <datalist id="employees">
                                <option></option>
                                @foreach($employee_list as $el)
                                    <option value="{{ $el->id }}">{{$el->first_name}} {{$el->last_name}}</option>
                                @endforeach
                            </datalist>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <strong><label>Payroll Dates</label></strong>
                            <input type="date" class="form-control" name="start_date">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <strong><label>-</label></strong>
                            <input type="date" class="form-control" name="end_date">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md">
                        <button class="btn btn-danger">Cancel</button>
                        <button class="btn btn-primary" type="submit">Create Payroll</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <br>
    <!--payroll details-->
    <div class="card" id="payroll_details" style="display:none">
        <div class="card-header">
            <h5>Payroll Details for Employee - John Dy</h5>
        </div>
        <div class="card-body">
            <div class="form-group">
                <div id="DivIdToPrint">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Payroll Date</th>
                                <th>Gross Earnings</th>
                                <th colspan="5">Deductions</th>
                                <th>Total Deductions</th>
                                <th>Net Pay</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">2019-09-29 to 2019-10-14</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">&#8369; 8000.00</td>
                                <td><strong>No. of Absent</strong></td>
                                <td><strong>No. of Late</strong></td>
                                <td><strong>PAGIBIG</strong></td>
                                <td><strong>Phil Health</strong></td>
                                <td><strong>SSS</strong></td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">&#8369; 2512.22</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">&#8369; 5487.78</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>1</td>
                                <td>&#8369; 300.00</td>
                                <td>&#8369; 200.00</td>
                                <td>&#8369; 765.64</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-danger">Cancel</button>
                <button class="btn btn-primary" onclick="printDiv()">Print Payroll Results</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')

@endsection

@section('scripts')
<script>
    function printDiv() 
    {

    var divToPrint=document.getElementById('DivIdToPrint');

    var newWin=window.open('','Print-Window');

    newWin.document.open();

    newWin.document.write('<html><style>table{border: 1px solid; border-collapse: collapse; text-align: center} tr td {border: 1px solid; border-collapse: collapse} th {border: 1px solid; border-collapse: collapse}</style><body onload="window.print()" style="text-align: center"><center><h3>John Dy payroll from 2019-09-29 to 2019-10-14</h3>'+divToPrint.innerHTML+'</center></body></html>');

    newWin.document.close();

    setTimeout(function(){newWin.close();},10);

    }

    $('#process_payroll').on('submit', function(e){
        $('#payroll_details').fadeIn();
        e.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            type: "post",
            url: "/employeepayroll/create_payroll",
            data: data, 
            success: function(response){
                $('#DivIdToPrint').html(response);
            },
            error: function(err){
                alert(err.responseTxt);
            }
        });
    });
</script>
@endsection