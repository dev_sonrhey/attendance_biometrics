@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <h5>Employee List</h5>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="employee_list">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Email Address</th>
                        <th>Mobile Number</th>
                        <th>Salary Rate (&#8369;)</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!--modal-->
<div class="modal fade" id="employee_details">
    <div class="modal-dialog">
        <form method="POST" action="{{URL('employeelist/update')}}">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <input type="hidden" name="employee_id">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Employee Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong><label>First Name</label></strong>
                                <input type="text" class="form-control" name="first_name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong><label>Middle Name</label></strong>
                                <input type="text" class="form-control" name="middle_name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong><label>Last Name</label></strong>
                                <input type="text" class="form-control"  name="last_name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong><label>Address</label></strong>
                                <input type="text" class="form-control"  name="address" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong><label>Email Address</label></strong>
                                <input type="text" class="form-control" name="email_address" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong><label>Mobile Number</label></strong>
                                <input type="text" class="form-control"  name="mobile_number" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <strong><label>Salary Rate</label></strong>
                                <input type="text" class="form-control"  name="salary_rate" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="controls" style="display:none">
                    <div class="modal-footer">
                            <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!--deductions-->
<div class="modal fade" id="deductions">
    <div class="modal-dialog">
        <form method="post" action="{{URL('employeelist/employee_deductions')}}">
            {{ csrf_field() }}
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Deductions</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="employee_id">
                <div class="form-group">
                    <strong><label>Employee Name</label></strong>
                    <input type="text" class="form-control" name="employee_name">
                </div>
                <div class="form-group">
                    <strong><label>PAGIBIG</label></strong>
                    <input type="text" class="form-control" name="pagibig">
                </div>
                <div class="form-group">
                    <strong><label>PHILHEALTH</label></strong>
                    <input type="text" class="form-control" name="philhealth">
                </div>
                <div class="form-group">
                    <strong><label>SSS</label></strong>
                    <input type="text" class="form-control" name="sss">
                </div>
                <div class="form-group">
                    <strong><label>Total Deductions</label></strong>
                    <input type="text" class="form-control" name="total_deductions" readonly>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Save Deductions</button>
                </div>
            </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/responsive.bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables/responsive.jqueryui.min.css') }}">
@endsection
@section('scripts')
<script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
    var load_employees = $('#employee_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/employeelist/load_employees',
        columns: [
            {data: 'first_name', name: 'first_name'},
            {data: 'middle_name', name: 'middle_name'},
            {data: 'last_name', name: 'last_name'},
            {data: 'address', name: 'address'},
            {data: 'email_address', name: 'email_address'},
            {data: 'mobile_number', name: 'mobile_number'},
            {data: 'salary_rate', name: 'salary_rate'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $(document).on('click', '#employee_view',function(){
        $('#employee_details').modal("show");
        var parent = $(this).closest('tr').find('td');
        $('[name="first_name"]').val(parent.eq(0).text()).attr("readonly", true);
        $('[name="middle_name"]').val(parent.eq(1).text()).attr("readonly", true);
        $('[name="last_name"]').val(parent.eq(2).text()).attr("readonly", true);
        $('[name="address"]').val(parent.eq(3).text()).attr("readonly", true);
        $('[name="email_address"]').val(parent.eq(4).text()).attr("readonly", true);
        $('[name="mobile_number"]').val(parent.eq(5).text()).attr("readonly", true);
        $('[name="salary_rate"]').val(parent.eq(6).text()).attr("readonly", true);
    });

    $(document).on('click', '#employee_edit',function(){
        $('#employee_details').modal("show");
        $('#controls').fadeIn();
        var employee_id = $(this).attr("data-employee-id");
        var parent = $(this).closest('tr').find('td');
        $('[name="first_name"]').val(parent.eq(0).text()).attr("readonly", false);
        $('[name="middle_name"]').val(parent.eq(1).text()).attr("readonly", false);
        $('[name="last_name"]').val(parent.eq(2).text()).attr("readonly", false);
        $('[name="address"]').val(parent.eq(3).text()).attr("readonly", false);
        $('[name="email_address"]').val(parent.eq(4).text()).attr("readonly", false);
        $('[name="mobile_number"]').val(parent.eq(5).text()).attr("readonly", false);
        $('[name="salary_rate"]').val(parent.eq(6).text()).attr("readonly", false);
        $('[name="employee_id"]').val(employee_id);
    });

    $(document).on('click', '#employee_deductions', function(){
        $('#deductions').modal("show");
        $('#controls').fadeIn();
        var employee_id = $(this).attr("data-employee-id");
        var parent = $(this).closest('tr').find('td');
        $('[name="employee_id"]').val(employee_id);
        $('[name="employee_name"]').val(parent.eq(0).text() + " " + parent.eq(2).text()).attr("readonly", true);
        // $('[name="first_name"]').val(parent.eq(0).text()).attr("readonly", false);
        // $('[name="middle_name"]').val(parent.eq(1).text()).attr("readonly", false);
        // $('[name="last_name"]').val(parent.eq(2).text()).attr("readonly", false);
        // $('[name="address"]').val(parent.eq(3).text()).attr("readonly", false);
        // $('[name="email_address"]').val(parent.eq(4).text()).attr("readonly", false);
        // $('[name="mobile_number"]').val(parent.eq(5).text()).attr("readonly", false);
        // $('[name="salary_rate"]').val(parent.eq(6).text()).attr("readonly", false);
        // $('[name="employee_id"]').val(employee_id);
    });

    $(document).on('keyup', '[name="pagibig"],[name="philhealth"],[name="sss"]', function(){
        var total = Number($('[name="pagibig"]').val()) + Number($('[name="philhealth"]').val()) + Number($('[name="sss"]').val());
        $('[name="total_deductions"]').val(total);
    });
</script>
@endsection