<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
/*Employee List*/
Route::get('employeelist/load_employees', 'EmployeeListController@load_employees');
Route::post('employeelist/employee_deductions', 'EmployeeListController@employee_deductions');
/*End Employee List*/

/*payroll*/
Route::post('employeepayroll/create_payroll', 'PayrollController@create_payroll');
/*End Payroll*/

/* RESOURCES HERE */
Route::resource('employeelist','EmployeeListController');
Route::resource('employeepayroll','PayrollController');
/* END RESOURCES HERE */


